import numpy as np
from runge_kutta4 import rk4


def gram_schmidt_orthogonalization(vectors):
    vectors = np.transpose(vectors)
    new_vectors = np.zeros(np.shape(vectors))
    vectors_norm = np.zeros(len(vectors))

    for vector_index, vector in enumerate(vectors):
        new_vectors[vector_index] = vector

        for second_vector_index in range(vector_index):
            second_vector = new_vectors[second_vector_index]
            new_vectors[vector_index] -= np.sum(vector * second_vector) * second_vector

        vectors_norm[vector_index] = np.sqrt(np.sum(new_vectors[vector_index] ** 2))
        if vectors_norm[vector_index] != 0:
            new_vectors[vector_index] /= vectors_norm[vector_index]

    return new_vectors.T, vectors_norm


def lyapunov_exponent(derivatives, jacobian, initial_condition, step_size, end_time, transient_time):
    N = len(derivatives)
    delta_Z = np.diag(np.ones(N))
    total_norms = np.zeros(N)

    times, Z_s = rk4(initial_condition, derivatives, step_size, transient_time, 0)
    times, Z_s = rk4(Z_s[-1], derivatives, step_size, end_time, times[-1])
    for step in range(len(times)):
        time = times[step]
        delta_z_dot = np.matmul(jacobian(time, Z_s[step]), delta_Z)
        delta_Z += delta_z_dot * step_size
        delta_Z, new_norms = gram_schmidt_orthogonalization(delta_Z)
        total_norms += np.log(new_norms)

    lambdas = total_norms / (times[-1] - times[0])
    return lambdas
