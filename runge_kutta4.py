#!/usr/bin/env python

""" modules to integrate using 4th order runge kutta method """

import numpy as np


def rk4(x_init, func, step, time, t_0=0):
    """ takes x and func as a vector of values and functions respectively """
    # find length
    length = len(x_init)

    func_iter = func_iter_wrapper(func)

    # initialization
    # store the record of x
    time = np.arange(t_0, time, step)
    x = np.zeros(shape=(len(time), length))
    x[0] = x_init

    # temporary values
    k1 = np.zeros(length)
    k2 = np.zeros(length)
    k3 = np.zeros(length)
    k4 = np.zeros(length)

    reduced_step = step / 6

    # evolve
    for i in range(len(time) - 1):
        # find k1
        func_iter(k1, time[i], x[i])

        # find k2
        func_iter(k2, time[i] + step / 2, x[i] + k1 * step / 2)

        # find k3
        func_iter(k3, time[i] + step / 2, x[i] + k2 * step / 2)

        # find k4
        func_iter(k4, time[i] + step, x[i] + step * k3)

        # update x
        x[i + 1] = x[i] + reduced_step * (k1 + 2 * (k2 + k3) + k4)
        time[i + 1] = time[i] + step

    return time, x


def func_iter_wrapper(func_list):
    def func_iter(out, *args):
        for func_index, func in enumerate(func_list):
            out[func_index] = func(*args)

    return func_iter


def x_dot(time, x):
    """ function to test algorithm """
    return x[1]


def y_dot(time, x):
    """ function to test algorithm """
    return -x[0]


def main():
    """ main body with the sole purpose of tesing the algorithm"""
    x_init = [1, 0]
    funcs = [x_dot, y_dot]
    end = 50
    step = 0.001

    time, x = rk4(x_init, funcs, step, end)

    import matplotlib.pyplot as plt

    plt.plot(time, x[:, 0], ls='--', label='position')
    plt.plot(time, x[:, 1], ls='-.', label='speed')
    plt.xlabel('time')
    plt.legend()
    plt.show()

    plt.plot(x[:, 0], x[:, 1], ls='-.')
    plt.show()

if __name__ == "__main__":
    main()
