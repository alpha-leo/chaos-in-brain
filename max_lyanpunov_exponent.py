from lyapunov_exponent import lyapunov_exponent
from model import Model
import numpy as np
import matplotlib.pyplot as plt


param = {
    'a_1': 3,
    'a_2': 9.8,
    'b_1': 2.2,
    'b_2': 2.2,
    'c_1': 2,
    'd': 10,
    'gamma_1': 0.1,
    'gamma_2': 0.1,
    'omega_1': 2 * np.pi,
    'omega_2': 2 * np.pi,
    'epsilon_1': 17.09,
    'epsilon_2': 2.99,
}
X_0 = [0.1, 0.6, 0.1, 0.8]
time = 20
step_size = .001
transient = 10

Omegas = np.linspace(0, 100, 1001)
exponents = np.zeros(len(Omegas))
for Omega_index, Omega in enumerate(Omegas):
    print(f'\rOmega: {Omega}', end='')
    model = Model(**param, Omega=Omega)
    derivatives = [model.x_dot, model.u_dot, model.y_dot, model.v_dot]
    exponents[Omega_index] = lyapunov_exponent(derivatives, model.jacobian, X_0, step_size, time, transient)[0]

data = {
    'Omegas': Omegas,
    'exponents': exponents
}
np.save(f'data/max_lyanpunov_exponents_{Omegas[0]}_{Omegas[-1]}_{len(Omegas)}_{transient}_{time}_{step_size}.npy', data)

plt.figure(figsize=(8, 4))
plt.plot(Omegas, exponents, linewidth=.5)
plt.xlabel(r'$\Omega$')
plt.ylabel(r'$\lambda_{max}$')
plt.savefig(f'results/max_lyanpunov_exponents_{Omegas[0]}_{Omegas[-1]}_{len(Omegas)}_{transient}_{time}_{step_size}.jpg')
plt.show()
